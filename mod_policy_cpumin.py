#!/usr/bin/python
import sys

def main(argv):
	sum = 0
	overload = False
	cpu_millis = 2000
	f = open('output.txt','wa')
	lines = sys.stdin.readlines()
	min_thresh = 200
	counter = 0
	remains = 0
	for line in lines:
		l = line.strip('\n')
		modules = l.split(":")
		if modules[0] != "policy" or modules[2] != "cpu":
			sys.stderr.write("There was a problem in the input\n")
			sys.exit(1)
		sum += int(modules[3])
		if int(modules[3]) <=  min_thresh:
			counter += 1 
	if sum >= cpu_millis:
		overload = True
	if overload:
		score = float(-30.5)
	else:
		remains = cpu_millis - sum
		score = float(42.6)
	print("score:"+str(score)+'\n')
	f.write("score:"+str(score)+'\n') 
	for line in lines:
		l = line.strip('\n')
		modules = l.split(":")
		if int(modules[3]) <=  min_thresh:
			print("set_limit:"+modules[1]+":cpu.shares:"+str((int(modules[3])+remains/counter)*1024/cpu_millis)+'\n')
			f.write("set_limit:"+modules[1]+":cpu.shares:"+str((int(modules[3])+remains/counter)*1024/cpu_millis)+'\n')
		else:
			print("set_limit:"+modules[1]+":cpu.shares:"+str(int(modules[3])*1024/cpu_millis)+'\n')
			f.write("set_limit:"+modules[1]+":cpu.shares:"+str(int(modules[3])*1024/cpu_millis)+'\n')
	f.close
if __name__ == "__main__":
   main(sys.argv[1:])
