#!/usr/bin/python
import sys
import os
import errno

def main(argv):
	wd = "/sys/fs/cgroup/cpu/monitor/"
        cpu_millis = 2000
        f = open('output_l.txt','w')
	lines = sys.stdin.readlines()
        for line in lines:
                l = line.strip('\n')
                modules = l.split(":")
                if str(modules[0]) != "set_limit" and str(modules[0]) != "create" and str(modules[0]) != "remove" and str(modules[0]) != "add":
                	sys.stderr.write("There was a problem in the input\n")
			sys.exit(1)
		elif modules[0] == "set_limit" and modules[2] == "cpu" and modules[4] == "cpu.shares":
			os.system("echo "+str(modules[5])+" > "+wd+str(modules[3])+"/cpu.shares")
		elif modules[0] == "add" and modules[2] == "cpu":
			os.system("echo "+str(modules[4])+" >> "+wd+str(modules[3])+"/tasks")
		elif modules[0] == "create" and modules[2] == "cpu":
			try:
        			os.makedirs(wd+str(modules[3]))
    			except OSError as exception:
       	 			if exception.errno != errno.EEXIST:
            				raise
		elif modules[0] == "remove" and modules[2] == "cpu":
			f_r = open(wd+str(modules[3])+"/tasks",'w')
			f_r.close
			os.rmdir(wd+str(modules[3]))	
		else:
			sys.stderr.write("There was a problem in the input\n")
	f.close
if __name__ == "__main__":
   main(sys.argv[1:])

